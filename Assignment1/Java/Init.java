import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;


public class Init {
	
	private static int size = 3583;
	private static Random rand = new Random();
	
	public static int[] generateRandom() {
		
		ArrayList<Integer> list = new ArrayList<Integer>(size);
		
		for(int i = 0; i < size; i++) {
			int randomValue;
			
			do {
				randomValue = rand.nextInt();
			} while (list.contains(randomValue));
			list.add(randomValue);
		}
	
		int[] toReturn = new int[size];
		int j = 0;
		for (Integer i : list) {
			toReturn[j] = i;
			j++;
		}
		return toReturn;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
