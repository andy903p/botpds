
public class Node {
	
	private Node left;
	private Node right;
	private int value;
	
	public boolean add(Node toAdd) {
		
		//left
		if(toAdd.getValue() < value) {
			if(left == null) {
				left = toAdd;
				return true;
			}
			else {
				left.add(toAdd);
				return true;
			}
		}
		else if(toAdd.getValue() > value) {
			if(right == null) {
				right = toAdd;
				return true;
			}
			else {
				right.add(toAdd);
				return false;
			}
		}
		else
			return false;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

}
