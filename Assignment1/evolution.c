#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <limits.h>
#include "evolution.h"


int fitness(int* tree)
{

    int errors, depth;

    depth = (int)(log(N)/log(2));

    for(int i = 0; i < depth; i++) {
        int elements = pow(2,i);
        for(int j = 0; j < elements; j++) {
            int current = ((int)pow(2,i))-1+j;
            if(((2*current)+1) >= N)
                continue;
            else if(tree[current] < tree[(2*current)+1])
                errors++;
            if(((2*current)+2) >=N)
                continue;
            else if(tree[current] > tree[(2*current)+2])
                errors++;
        }
    }

    return errors;

}

int* mutation(int* old_solution, int size)
{
    srand(time(NULL));
    int a,b,tmp;
    int* new_solution = copy_array(old_solution,size);

    for(int i = 0; i < 100; i++) {
        a = rand() % size;
        b = rand() % size;
        tmp = new_solution[a];
        new_solution[a] = new_solution[b];
        new_solution[b] = tmp;
    }
    return new_solution;
}

int* construct_check_array(int* array,int size)
{
    int* retVal = malloc(sizeof(int)*MAXRANDOM);

    for(int i = 0; i < MAXRANDOM; i++) {
        retVal[i] = 0;
    }
    for(int i = 0; i < size; i++) {
        retVal[array[i]]++;
    }

    return retVal;
}

int* recombination(int* a, int* b, int size, int* check)
{
    srand(time(NULL));
    int* new_array = copy_array(a,size);

    int depth = (int)(log(size)/log(2))+1;

    int tree_depth = rand() % depth;
    int tree_equal_elements = pow(2,tree_depth);
    int tree_pos_j = rand() % tree_equal_elements;
    int tree_pos_in_array = tree_equal_elements-1 + tree_pos_j;

    insert_tree(size,tree_pos_in_array, b, new_array);

    for(int i = 0; i < size; i++) {
        check[new_array[i]]--;
    }

    for(int i = 0; i < MAXRANDOM; i++) {
        if(check[i] < 0) {
            for(int j = 0; j < MAXRANDOM; j++) {
                if(check[j] > 0) {
                    for(int k = 0; k < size; k++) {
                        if(new_array[k] == i) {
                            new_array[k] = j;
                            check[i]++;
                            check[j]--;
                        }
                    }
                }
            }
        }
    }

    return new_array;


}

void insert_tree(int max_size, int pos, int* source, int* target)
{
    if(pos >= max_size)
        return;
    target[pos] = source[pos];
    insert_tree(max_size,2*pos+1,source,target);
    insert_tree(max_size,2*pos+2,source,target);
}

int* copy_array(int* source, int n)
{
    int* target = malloc(n*sizeof(int));

    for(int i = 0; i < n; i++)
        target[i] = source[i];

    return target;
}

int* copy_array_random(int* source, int n)
{
    int* target = malloc(n*sizeof(int));
    srand(time(NULL));
    int random_offset = rand() % N;
    for(int i = 0; i < n; i++){
        if((i+random_offset)<n)
            target[i] = source[i+random_offset];
        else
            target[i] = source[(random_offset+i)-n];
    }

    return target;
}

int array_contains(int* arr, int val)
{
    for(int i = 0; i < N; i++) {
        if(arr[i] == val)
            return 1;
    }

    return 0;

}

int* constructArray()
{

    int* newArray = calloc(N,sizeof(int));


    memset(newArray,0,N);
    srand(time(NULL));

    int random_value;

    for(int i = 0; i < N; i ++) {
        random_value = rand()%MAXRANDOM;
        while(array_contains(newArray,random_value))
            random_value = (random_value+1)%MAXRANDOM;

        newArray[i] = random_value;
    }
    printf("finished constructing array\n");
    return newArray;

}

int * makecheckarray(int* tree, int size)
{
    int* retVal = malloc(sizeof(int)*MAXRANDOM);
    memset(retVal,0,MAXRANDOM);

    for(int i = 0; i < size; i++)
        retVal[tree[i]]++;
    return retVal;
}

int* iteration()
{
    int * result;
    //random population
    int ** population = malloc(sizeof(int*)*POPSIZE);
    int ** offsprings = malloc(sizeof(int*)*POPSIZE);
    int ** mutations = malloc(sizeof(int*)*POPSIZE);
    int * eval = malloc(2*POPSIZE*sizeof(int));
    memset(eval,INT_MAX,2*POPSIZE);


    population[0] = constructArray();
    for(int i = 1; i<POPSIZE; i++)
        population[i] = copy_array_random(population[0],N);

    int*check = makecheckarray(population[0],N);

    int end = 0;

    int iteration = 0;
    while(!end) {
        srand(time(NULL));
        //make love
        for(int i = 0; i<POPSIZE; i++) {
            int dad, mum;
            dad = rand()%POPSIZE;
            do {
                mum = rand()%POPSIZE;
            } while(mum==dad);
            #ifdef DEBUG
            printf("Just parents making sweet love!\n");
            #endif
            int * mycheck = copy_array(check,MAXRANDOM);
            offsprings[i] = recombination(population[dad],population[mum],N,mycheck);
            free(mycheck);
        }
        //the world is hard, FUKUSHIMA
        for(int i = 0; i<POPSIZE; i++) {
            #ifdef DEBUG
            printf("The radiation is too high!\n");
            #endif
            mutations[i] = mutation(offsprings[i],N);
        }

        //evaluation
        for(int i = 0; i<2*POPSIZE; i++) {
            #ifdef DEBUG
            printf("Run! show us your fitness!\n");
            #endif
            if(i<POPSIZE)
                eval[i] = fitness(population[i]);
            else
                eval[i] = fitness(mutations[i-POPSIZE]);
        }

        //selection
        for(int i = 0; i<2*POPSIZE; i++) {
            for(int j = i+1; j<2*POPSIZE; j++) {
                if(eval[j]<eval[i]) {
                    int auxV = eval[i];
                    eval[i] = eval[j];
                    eval[j] = auxV;
                    int * auxIndividual;
                    if(i<POPSIZE && j<POPSIZE) {
                        auxIndividual = population[i];
                        population[i] = population[j];
                        population[j] = auxIndividual;
                    } else if(i>=POPSIZE && j<POPSIZE) {
                        auxIndividual = mutations[i-POPSIZE];
                        mutations[i-POPSIZE] = population[j];
                        population[j] = auxIndividual;
                    } else if(i<POPSIZE && j>=POPSIZE) {
                        auxIndividual = population[i];
                        population[i] = mutations[j-POPSIZE];
                        mutations[j-POPSIZE] = auxIndividual;

                    } else if(i>=POPSIZE && j>=POPSIZE) {
                        auxIndividual = mutations[i-POPSIZE];
                        mutations[i-POPSIZE] = mutations[j-POPSIZE];
                        mutations[j-POPSIZE] = auxIndividual;
                    }
                }
            }
        }
        int min = INT_MAX;
        for(int i = 0; i<2*POPSIZE; i++) {
            if(eval[i] < min)
                min = eval[i];
            if(eval[i] == 0) {
                end = 1;
                if(i<POPSIZE)
                    result = copy_array(population[i],N);
                else
                    result = copy_array(mutations[i-POPSIZE],N);
                break;
            }
        }

        for(int i=0; i<POPSIZE; i++) {
        free(mutations[i]);
        free(offsprings[i]);
        }
        printf("iteration %d: best fitness %d\n",iteration,min);
        iteration++;
    }

    for(int i=0; i<POPSIZE; i++) {
        free(population[i]);
    }
    free(population);
    free(offsprings);
    free(mutations);

    return result;
}
