#ifndef EVOLUTION_H
#define EVOLUTION_H

//#define N 16
#define N 100
#define MAXRANDOM 10000
#define POPSIZE 50

int fitness(int* tree);

int* mutation(int* old_solution, int size);

int* construct_check_array(int* array,int size);

int* recombination(int* a, int* b, int size, int* check);

void insert_tree(int max_size, int pos, int* source, int* target);

int* copy_array(int* source, int n);

int array_contains(int* arr, int val) ;

int* constructArray();

int * makecheckarray(int* tree, int size);

int* iteration();


#endif // EVOLUTION_H
