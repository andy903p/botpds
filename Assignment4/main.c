#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <float.h>
#include <math.h>

#define N 48
#define ANTS 48

double edges[N][N];
double pheromone[N][N];
static int way[ANTS][N];
double global_best = DBL_MAX;
double evaporation_factor = 0.95;

double euklid_dist_2d(double a[2], double b[2]) {	
	return sqrt( pow(b[0] - a[0],2) + pow(b[1] - a[1],2));
}

void read_fill_distances(FILE* file) {
	
}

void fill_distances(double positions[N][2]) {
	for(int i = 0; i < N; i++) {
		for(int j = 0; j < N; j++) {
			edges[i][j] = euklid_dist_2d(positions[i],positions[j]);
		}
	}
}

void init_pheromone() {
    for(int i = 0; i< N; i++)
        for(int j = 0; j < N; j++)
            pheromone[i][j] = 1;
}

void add_pheromone(int ant, int best) {
	for(int i = 0; i < N; i++) {
		pheromone[way[ant][i]][way[ant][(i+1)%N]] += 1*best;
	}
}

void evaporate() {
	for(int i = 0; i < N; i++) {
		for(int j = 0; j < N; j++) {
			pheromone[i][j] *= evaporation_factor;
		}
	}
}

int bestpath() {
	double tmp[ANTS], best = DBL_MAX;
	int bestPos;
	for(int i = 0; i < ANTS; i++) {
		for(int j = 0; j < N; j++) {
			tmp[i] += way[i][j] + way[i][(j+1)%N];
		}
	}
	
	for(int i = 0; i < ANTS; i++) {
		if(best > tmp[i]) {
			best = tmp[i];
			bestPos = i;
		}
	}
	
	if(best < global_best) {
		global_best = best;
	}
	
	return bestPos;
	
}

void distribute_ants() {
    srand(time(NULL));
    for(int i = 0; i < ANTS; i++) {
        way[i][0] = rand()%N;
    }
}

void print_way() {
	
    for(int i = 0; i < ANTS; i++) {
		printf("Ant %d:\t",i);
		for(int j = 0; j < N; j++) {
			printf("%d ",way[i][j]);
		}
		printf("\n");
    }
}

int alreadyused(int ant, int city) {
    for(int i = 0; i < N; i++) {
        if(way[ant][i] == city)
            return 1;
    }
    return 0;
}

double sum_otherpheromone(int pos) {
    double retVal;
    for(int i = 0; i < N; i++) {
        if(i == pos)
            continue;
        retVal += pheromone[pos][i];
    }
    return retVal;
}

int choosenext(int position,int ant) {
    
    srand(time(NULL));

    int next_city = -1;

    do {

    double chosen = (double)((double)(rand()%100))/100, prop, prop_inv = 0.0,propall;
    propall = sum_otherpheromone(position);

    for(int i = 0; i  < N; i++) {
        if(i == position)
            continue;
        prop = pheromone[position][i]/propall;
        prop_inv += prop;
        if(chosen < prop_inv)
            next_city = i;
    }
    }while(alreadyused(ant, next_city));

    return next_city;


}

void *ant(void* data) {

    int id = (int)(*data);
    for(int position = 0; position < N-1; position++) {
        choosenext(way[id][position],id);
    }

    return 0;
}

int main(int argc, char **argv) {

    int n, i;
    FILE* file;
    pthread_t *threads;

    if(argc != 2) {
        fprintf(stderr, "Usage: %s <Filename>\n",argv[0]);
        return EXIT_FAILURE;
    }
    else if(!(file = fopen(argv[1],"r"))) {
            perror("File does not exist");
            return EXIT_FAILURE;
    }
    
    read_fill_distances(file);

    init_pheromone();
    distribute_ants();
    print_way();
	
	for(int i = 0; i < ANTS; i++) {
		ant();
	}
	
    /*
    threads = malloc(n * sizeof(pthread_t));

    for(i = 0; i < n; i++) {
        pthread_create(threads+i,NULL,&ant,NULL);
    }

    for(i = 0; i < n; i++) {
        pthread_join(threads[i],NULL);
    }
    free(threads);
    */
    return EXIT_SUCCESS;
}
