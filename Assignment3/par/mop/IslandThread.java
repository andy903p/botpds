package mop;

import java.util.List;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;

import ea.Evolution;

public class IslandThread implements Runnable {

	private List<double[]> population;
	private double[] bestSolutions;
	private double[] z;
	private int[] indices;
	private List<Objective> objectives;
	private List<Subproblem> problems;
	private LinkedBlockingQueue<double[]> EP;

	public IslandThread(int[] indices, List<double[]> population,
			double[] bestSolutions, double[] z, List<Objective> objectives,
			List<Subproblem> problems, LinkedBlockingQueue<double[]> EP) {
		this.indices = indices;
		this.population = population;
		this.bestSolutions = bestSolutions;
		this.z = z;
		this.objectives = objectives;
		this.problems = problems;
		this.EP = EP;

	}

	@Override
	public void run() {
		Random r = new Random();

		int mother = r.nextInt(indices.length), father;
		do {
			father = r.nextInt(indices.length);
		} while (mother == father);
		double[] child = Evolution.crossover(population.get(indices[mother]),
				population.get(indices[father]));
		child = Evolution.mutate(child);
		// improvement
		// update z
		for (int j = 0; j < z.length; j++) {
			double fitness = objectives.get(j).fitness(child);
			if (fitness < z[j])z[j] = fitness;
		}
		// update neighboring solutions
		for (int j = 0; j < indices.length; j++) {
			Subproblem curr = problems.get(indices[j]);
			if (curr.fitness(child) < curr.fitness(population.get(indices[j]))) {
				population.set(indices[j], child);
				bestSolutions[indices[j]] = curr.fitness(child);
			}
		}
		
		LinkedBlockingQueue<double[]> EPtmp = new LinkedBlockingQueue<>();
		
		double[] tmp = EP.poll();
		while(tmp != null) {
//			System.out.println("Ping");
			EPtmp.add(tmp);
			tmp = EP.poll();
		}
		// update EP
		tmp = EPtmp.poll();
		double[] F = calculateF(child);
		boolean dominated = false;
		while (tmp != null) {
//			System.out.println("Pong");
			if (Utility.dominate(F, tmp)) {
				//System.out.println("Replacing");
			}
			else if (Utility.dominate(tmp, F)) {
				dominated = true;
				EP.add(tmp);
			}
			else {
				//System.out.println("Tataaa");
			}
			tmp = EPtmp.poll();
		}
		if (!dominated){
			EP.add(F);
//			System.out.println("Yeah " + EP.size());
		}
	}

	private double[] calculateF(double[] x) {
		double[] result = new double[objectives.size()];
		// double [] result = new double [N];
		// for(int i = 0; i<N; i++)
		// result[i] = problems.get(i).fitness(x);
		for (int i = 0; i < objectives.size(); i++)
			result[i] = objectives.get(i).fitness(x);
		return result;
	}

}
