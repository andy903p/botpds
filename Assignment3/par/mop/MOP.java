package mop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import mop.objectives.F1;
import mop.objectives.F2;
import ea.Evolution;

public class MOP {
	private int N;
	private int dimension;
	private LinkedBlockingQueue<double[]>[] EP;
	private List<int[]> nearestNeighbors;// B
	private List<Subproblem> problems;
	private List<Objective> objectives;
	private double[] bestSolutions; // FVi
	private double[] z;// curren best solutions
	private List<double[]> population;
	private List<double[]>[] islandPopulations;

	public MOP(int N, List<Objective> objectives, int dimension, int islandSize) {
		this.N = N;
		this.dimension = dimension;
		this.objectives = objectives;
		this.problems = new ArrayList<Subproblem>(N);
		this.bestSolutions = new double[N];
		this.EP = new LinkedBlockingQueue[N];
		for (int i = 0; i < N; i++)
			this.EP[i] = new LinkedBlockingQueue<double[]>();
		initZ();
		this.population = Evolution.generatePopulation(N, dimension);
		this.islandPopulations = new List[N];
		for (int i = 0; i < N; i++) {
			islandPopulations[i] = new LinkedList<double[]>();
			for (double[] item : population) {
				islandPopulations[i].add(item.clone());
			}
		}
		for (int i = 0; i < N; i++) {
			// init problems
			problems.add(new Subproblem(objectives));
			// initialize best solutions of subproblems
			bestSolutions[i] = problems.get(i).fitness(population.get(i));
		}

		// initializeIslands(subProblems,islandSize);
		this.nearestNeighbors = new ArrayList<int[]>(N);
		for (int i = 0; i < N; i++) {
			List<Subproblem> tmp = Utility.findNearestNeighbors(problems,
					islandSize, new ComparatorSubproblem(problems.get(i)));
			int[] indices = new int[islandSize];
			for (int j = 0; j < islandSize; j++)
				indices[j] = problems.indexOf(tmp.get(j));
			nearestNeighbors.add(indices);
		}
	}

	private void initZ() {
		z = new double[objectives.size()];
		for (int i = 0; i < objectives.size(); i++)
			z[i] = Double.MAX_VALUE;
	}

	public void update() {
		RejectedExecutionHandler handler = new ThreadPoolExecutor.CallerRunsPolicy();
		ThreadPoolExecutor exec = new ThreadPoolExecutor(8, 8, 3L,
				TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>(), handler);
		for (int i = 0; i < N; i++) {
			// reproduction
			int[] indices = nearestNeighbors.get(i);
			Runnable tmp = new IslandThread(indices, islandPopulations[i],
					bestSolutions, z, objectives, problems, EP[i]);
			exec.execute(tmp);

		}
		exec.shutdown();
	}

	public List<double[]> iteration(int desiredEPSize) {
		int iteration = 0;
		int EPSize = 0;
		int inv = 0;
		List<double[]> EPall = new LinkedList<double[]>();
		do {

			while (EPSize < desiredEPSize) {
				System.out.println(iteration + ": " + EPSize);
				update();
				iteration++;
				if(EPall.size() == 0) {
				EPSize = 0;
				for (int i = 0; i < N; i++) {
					EPSize += EP[i].size();
					// System.out.println(EP[i].size());
				}
				}else
					EPSize = EPall.size();
				if (inv % 50 == 0) {
					for (int i = 0; i < islandPopulations.length; i++) {
						
						migrate(islandPopulations[i], islandPopulations[(i+1)%islandPopulations.length]);
					}
				}
				if ((inv++ % 5) == 0) {
					communicate(EPall);
				}
			}
			System.out.println("Merging");
			communicate(EPall);
			EPSize = EPall.size();
		} while (EPall.size() < desiredEPSize);
		return EPall;
	}

	private void migrate(List<double[]> island1, List<double[]> island2) {
		double[] tmp;
		for (int i = 0; i < island1.size(); i++) {
			Subproblem prob = problems.get(i);
			tmp = prob.fitness(island1.get(i)) < prob.fitness(island2
					.get(i)) ? island1.get(i) : island2.get(i);
			island1.set(i, tmp);
			island2.set(i, tmp);
		}
	}

	private/* List<double[]> */void communicate(List<double[]> EPall) {
		// List<double[]> EPall = new LinkedList<double[]>();
		for (int i = 0; i < N; i++) {
			for (double[] item : EP[i]) {
				Iterator<double[]> it = EPall.iterator();
				boolean dominated = false;
				while (it.hasNext()) {
					double[] next = it.next();
					if (Utility.dominate(item, next))
						it.remove();
					else if (Utility.dominate(next, item))
						dominated = true;
				}
				if (!dominated) {
					if (!EPall.contains(item))
						EPall.add(item);
				}
			}

		}
		// Communication between Islands
		for (int i = 0; i < EP.length; i++) {
			this.EP[i] = new LinkedBlockingQueue<double[]>();
			for (double[] item : EPall) {
				EP[i].add(item);
			}
		}
		// System.out.println(EPall.size());
		// return EPall;
	}

	// public void initializeIslands(List<Subproblem> problems, int size){
	// islands = new LinkedList<Island>();
	// Random r = new Random();
	// int chief;
	// do{
	// chief = r.nextInt(problems.size());
	// final Subproblem tmp = problems.get(chief);
	// List<Subproblem> neighbors = Utility.findNearestNeighbors(problems, size,
	// new Comparator<Subproblem>(){
	// @Override
	// public int compare(Subproblem arg0, Subproblem arg1) {
	// if(arg0.distance(tmp) < arg1.distance(tmp))
	// return -1;
	// if(arg0.distance(tmp) > arg1.distance(tmp))
	// return 1;
	// return 0;
	// }
	// });
	// islands.add(new Island(neighbors,dimension,population));
	// for(Subproblem curr: neighbors)
	// problems.remove(curr);
	// }while(!problems.isEmpty());
	// }

	public int getN() {
		return N;
	}

	public void setN(int n) {
		N = n;
	}

	public int getDimension() {
		return dimension;
	}

	public void setDimension(int dimension) {
		this.dimension = dimension;
	}

	public List<Objective> getObjectives() {
		return objectives;
	}

	public void setObjectives(List<Objective> objectives) {
		this.objectives = objectives;
	}

	public List<int[]> getNearestNeighbors() {
		return nearestNeighbors;
	}

	public void setNearestNeighbors(List<int[]> nearestNeighbors) {
		this.nearestNeighbors = nearestNeighbors;
	}

	public List<Subproblem> getProblems() {
		return problems;
	}

	public void setProblems(List<Subproblem> problems) {
		this.problems = problems;
	}

	public double[] getBestSolutions() {
		return bestSolutions;
	}

	public void setBestSolutions(double[] bestSolutions) {
		this.bestSolutions = bestSolutions;
	}

	public double[] getZ() {
		return z;
	}

	public void setZ(double[] z) {
		this.z = z;
	}

	public List<double[]> getPopulation() {
		return population;
	}

	public void setPopulation(List<double[]> population) {
		this.population = population;
	}

	public static void main(String[] args) {
		List<Objective> objectives = new LinkedList<Objective>();
		objectives.add(new F1());
		objectives.add(new F2());
		MOP mop = new MOP(1000, objectives, 30, 25);
		List<double[]> EP = mop.iteration(1000);
		System.out.println(EP.size());
		System.out.println(Arrays.toString(mop.getZ()));
		for (double[] curr : EP)
			System.out.println(Arrays.toString(curr));
		Utility.printParetoFrontToFile(
				"/home/andy/result1000decomp_5isl_25_dim30_pf1000.dat",
				EP);
	}

}
