package mop;

import java.util.Comparator;

public class ComparatorSubproblem implements Comparator<Subproblem> {
	private Subproblem reference;
	
	public ComparatorSubproblem(Subproblem reference){
		this.reference = reference;
	}
	
	@Override
	public int compare(Subproblem arg0, Subproblem arg1) {
		if(arg0.distance(reference) < arg1.distance(reference))
			return -1;
		if(arg0.distance(reference) > arg1.distance(reference))
			return 1;
		return 0;
	}
}
