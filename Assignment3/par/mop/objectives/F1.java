package mop.objectives;

import mop.Objective;

public class F1 implements Objective {

	@Override
	public double fitness(double[] input) {
		double result = input[0];
		double help = 0;
		int jSize = 0;
		for(int i = 2; i<=input.length;i++)
			if(i%2 == 1){
				help += Math.pow((input[i-1]-Math.pow(input[0], 0.5*(1.0+((3.*((double)i-3.))/((double)input.length-2.))))),2.);
				jSize++;
			}
		help /= (double) jSize;
		result += help;
		return result;
	}

}
