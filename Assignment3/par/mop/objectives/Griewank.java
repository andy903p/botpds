package mop.objectives;

import mop.Objective;

public class Griewank implements Objective{

	@Override
	public double fitness(double[] pos) {
		double auxS = 0;
		double auxP = 1;
		double fitness = 0;
		int dim = pos.length;
		for (int i = 1; i <= dim; i++) {
			auxS += Math.pow(pos[i-1], 2);
			auxP *= Math.cos(pos[i-1]/Math.sqrt(i));
		}
		fitness = (auxS/4000.d) - auxP + 1.0;		
		return fitness;
	}

}
