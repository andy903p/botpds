package mop;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class Subproblem {
	private int objectiveCount;
	private double [] weights;
	private List<Objective> objectives;
	private List<Subproblem> neighbors;
	
	public Subproblem(List<Objective> objectives){
		this.objectiveCount = objectives.size();
		this.objectives = objectives;
		this.weights = Utility.generateWeightVector(objectiveCount);
	}
	
	/**
	 * This fitness function uses the weighted sum approach to decompose the multi-object-optimization problem 
	 * @param input
	 * @return the fitness of the MOP given the weight vector of this SubProblem.
	 */
	public double fitness(double [] input){
		double result = 0;
		for(int i = 0; i<objectiveCount; i++)
			result += weights[i] * objectives.get(i).fitness(input);
		return result;
	}
	
	/**
	 * The distance according to the weight vectors, this is necessary to determine the neighborhood structure.
	 * @param other another SubProblem
	 * @return the distance to the given SubProblem.
	 */
	public double distance(Subproblem other){
		double dist = 0;
		for(int i = 0; i<objectiveCount; i++)
			dist += Math.pow(this.weights[i] - other.getWeights()[i], 2);
		dist = Math.sqrt(dist);
		return dist;
	}
	
	
	/**
	 * Initializes the nearest neighbors of the subproblem.
	 * @param amount
	 * @param candidates
	 */
	public void initializeNeighbors(int amount, List<Subproblem> candidates){
		this.neighbors = Utility.findNearestNeighbors(candidates, amount, new Comparator<Subproblem>(){
			@Override
			public int compare(Subproblem arg0, Subproblem arg1) {
				if(distance(arg0) < distance(arg1))
					return -1;
				else if(distance(arg0) > distance(arg1))
					return 1;
				return 0;
			}
		});
	}

	public int getObjectiveCount() {
		return objectiveCount;
	}

	public void setObjectiveCount(int objectiveCount) {
		this.objectiveCount = objectiveCount;
	}

	public double[] getWeights() {
		return weights;
	}

	public void setWeights(double[] weights) {
		this.weights = weights;
	}

	public List<Objective> getObjectives() {
		return objectives;
	}

	public void setObjectives(List<Objective> objectives) {
		this.objectives = objectives;
	}

	public List<Subproblem> getNeighbors() {
		return neighbors;
	}

	public void setNeighbors(List<Subproblem> neighbors) {
		this.neighbors = neighbors;
	}

	public void printNeighbors() {
		System.out.println("neighbors of "+Arrays.toString(weights)+":");
		for(Subproblem curr: neighbors)
			System.out.println(Arrays.toString(curr.getWeights()));
	}
	
	public void print() {
		System.out.println(Arrays.toString(weights));
	
	}
	

}
