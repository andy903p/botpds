package mop.objectives;

import mop.Objective;

public class F2 implements Objective {

	@Override
	public double fitness(double[] input) {
		double result = 1 - Math.sqrt(input[0]);
		double help = 0;
		int jSize = 0;
		for(int i = 2; i<=input.length;i++)
			if(i%2 == 0){
				help += Math.pow((input[i-1]-Math.pow(input[0], 0.5*(1.0+((3.*((double)i-3.))/((double)input.length-2.))))),2.);
				jSize++;
			}
		help *= (2./(double) jSize);
		result += help;
		return result;
	}

}
