package mop;

public interface Objective {
	public double fitness(double[] input);
}
