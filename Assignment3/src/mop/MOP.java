package mop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import ea.Evolution;

import mop.objectives.F1;
import mop.objectives.F2;

public class MOP {
	private int N;
	private int dimension;
	private List<double []> EP;
	private List<int []> nearestNeighbors;//B
	private List<Subproblem> problems;
	private List<Objective> objectives;
	private double [] bestSolutions; //FVi
	private double [] z;//curren best solutions
	private List<double []> population;
	
	public MOP(int N, List<Objective> objectives, int dimension, int islandSize){
		this.N = N;
		this.dimension = dimension;
		this.objectives = objectives;
		this.problems = new ArrayList<Subproblem>(N);
		this.bestSolutions = new double[N];
		this.EP = new LinkedList<double []>();
		initZ();
		this.population = Evolution.generatePopulation(N, dimension);
		
		for(int i = 0;i<N;i++){
			//init problems
			problems.add(new Subproblem(objectives));
			//initialize best solutions of subproblems
			bestSolutions[i] = problems.get(i).fitness(population.get(i));
		}
		
		//initializeIslands(subProblems,islandSize);
		this.nearestNeighbors = new ArrayList<int[]>(N);
		for(int i = 0; i < N; i++){
			List<Subproblem> tmp = Utility.findNearestNeighbors(problems, islandSize, new ComparatorSubproblem(problems.get(i)));
			int [] indices = new int[islandSize];
			for(int j = 0; j<islandSize; j++)
				indices[j] = problems.indexOf(tmp.get(j));
			nearestNeighbors.add(indices);
		}
	}
	
	private void initZ() {
		z = new double[objectives.size()];
		for(int i = 0;i<objectives.size(); i++)
			z[i] = Double.MAX_VALUE;
	}
	
	public void update(){
		Random r = new Random();
		for(int i = 0; i<N; i++){
			//reproduction
			int [] indices = nearestNeighbors.get(i);
			int mother = r.nextInt(indices.length), father; 
			do{
				father = r.nextInt(indices.length);
			}while(mother == father);
			double [] child = Evolution.crossover(population.get(indices[mother]), population.get(indices[father]));
			child = Evolution.mutate(child);
			//improvement
			//TODO:
			//update z
			for(int j = 0; j<z.length; j++){
				double fitness = objectives.get(j).fitness(child);
				if(fitness < z[j])
					z[j] = fitness; 
			}
			//update neighboring solutions
			for(int j = 0; j<indices.length; j++){
				Subproblem curr = problems.get(indices[j]);
				if(curr.fitness(child) < curr.fitness(population.get(indices[j]))){
					population.set(indices[j], child);
					bestSolutions[indices[j]] = curr.fitness(child);
				}
			}
			//update EP
			Iterator<double []> it = EP.iterator();
			double [] F = calculateF(child);
			boolean dominated = false;
			while(it.hasNext()){
				double [] next = it.next();
				if(Utility.dominate(F, next))
					it.remove();
				else if(Utility.dominate(next, F))
					dominated = true;
			}
			if(!dominated)
				EP.add(F);
		}
	}

	public List<double []> iteration(int desiredEPSize){
		int iteration = 0;
		while(EP.size() < desiredEPSize){
			System.out.println(iteration+": "+EP.size());
			update(); 
			iteration++;
		}
		return EP;
	}
	
	private double[] calculateF(double [] x) {
		double [] result = new double [N];
		for(int i = 0; i<N; i++)
			result[i] = problems.get(i).fitness(x);
		return result;
	}
	
//	public void initializeIslands(List<Subproblem> problems, int size){
//		islands = new LinkedList<Island>();
//		Random r = new Random();
//		int chief;
//		do{
//			chief = r.nextInt(problems.size());
//			final Subproblem tmp = problems.get(chief);
//			List<Subproblem> neighbors = Utility.findNearestNeighbors(problems, size, new Comparator<Subproblem>(){
//				@Override
//				public int compare(Subproblem arg0, Subproblem arg1) {
//					if(arg0.distance(tmp) < arg1.distance(tmp))
//						return -1;
//					if(arg0.distance(tmp) > arg1.distance(tmp))
//						return 1;
//					return 0;
//				}
//			});
//			islands.add(new Island(neighbors,dimension,population));
//			for(Subproblem curr: neighbors)
//				problems.remove(curr);
//		}while(!problems.isEmpty());
//	}
	
	
	public int getN() {
		return N;
	}

	public void setN(int n) {
		N = n;
	}

	public int getDimension() {
		return dimension;
	}

	public void setDimension(int dimension) {
		this.dimension = dimension;
	}

	public List<Objective> getObjectives() {
		return objectives;
	}

	public void setObjectives(List<Objective> objectives) {
		this.objectives = objectives;
	}


	public List<double[]> getEP() {
		return EP;
	}

	public void setEP(List<double[]> eP) {
		EP = eP;
	}

	public List<int[]> getNearestNeighbors() {
		return nearestNeighbors;
	}

	public void setNearestNeighbors(List<int[]> nearestNeighbors) {
		this.nearestNeighbors = nearestNeighbors;
	}

	public List<Subproblem> getProblems() {
		return problems;
	}

	public void setProblems(List<Subproblem> problems) {
		this.problems = problems;
	}

	public double[] getBestSolutions() {
		return bestSolutions;
	}

	public void setBestSolutions(double[] bestSolutions) {
		this.bestSolutions = bestSolutions;
	}

	public double[] getZ() {
		return z;
	}

	public void setZ(double[] z) {
		this.z = z;
	}

	public List<double[]> getPopulation() {
		return population;
	}

	public void setPopulation(List<double[]> population) {
		this.population = population;
	}

	public static void main(String [] args){
		List<Objective> objectives = new LinkedList<Objective>();		
		objectives.add(new F1());
		objectives.add(new F2());
		MOP mop = new MOP(100,objectives,30,10);
		List<double []> EP = mop.iteration(200);
		System.out.println(Arrays.toString(mop.getZ()));
//		for(double [] curr : EP)
//			System.out.println(Arrays.toString(curr));
	}
}
