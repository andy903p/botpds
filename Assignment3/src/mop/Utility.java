package mop;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.TreeSet;

public class Utility{
	
	/**
	 * Generates a weight vector (sum of it's elements == 1) of
	 * a given length
	 * @param size
	 * @return weight vector
	 */
	public static double [] generateWeightVector(int size){
		Random r = new Random();
		double [] result = new double[size];
		double sum = 0;
		for(int i = 0; i<size-1; i++){
			double next;
			int counter = 0;
			do{
				next = r.nextDouble();
				counter++;
			}while(sum + next > 1 || counter > 100);
			
			if((sum + next)<=1){
				result[i] = next;
				sum += next;
			}
			else
				result[i] = 0;
		}
		result[size-1] = 1-sum;
		return result;
	}
	
	/**
	 * Compares a and b.
	 * 
	 * @return true if a dominates b false else
	 */
	public static boolean dominate(double[] a, double[] b) {
		int size = a.length;
		boolean result = true;
		for (int i = 0; i < size; i++) {
			if (a[i] > b[i])
				result = false;
		}
		if (result == true)
			for (int i = 0; i < size; i++)
				if (a[i] < b[i])
					result = true;
		return result;
	}

	/**
	 * Finds the nearest neighbors of objects of type T according to a given comparator.
	 * @param database all possible neighbors.
	 * @param amount the desired amount of neighbors.
	 * @param comparator the comparator which is used to obtain the nearest neighbors.
	 * @return a list of 'amount' nearest neighbors.
	 */
	public static <T> List<T> findNearestNeighbors(List<T> database, int amount, Comparator<T> comparator) {
		List<T> results = new LinkedList<T>();
		TreeSet<T> tree = new TreeSet<T>(comparator);
		for (T curr : database) {
			if (tree.size() <= amount) {
				tree.add(curr);
				continue;
			}
			else if (comparator.compare(tree.last(),curr) > 0) {
				tree.remove(tree.last());
				tree.add(curr);
			}
		}
		for (int i = 0; i < amount; i++)
			results.add(tree.pollFirst());
		return results;
	}
}


