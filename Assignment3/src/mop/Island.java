package mop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ea.Evolution;

public class Island {
	private int size, dimension;
	private List<Subproblem> problems;
	private List<Evolution> eas;
	private List<double []> initialPopulation;
	private double[][] solutions;
	
	public Island(List<Subproblem> problems, int dimension, List<double[]> initial){
		this.size = problems.size();
		this.dimension = dimension;
		this.problems = problems;
		this.solutions = new double[problems.size()][this.dimension];
		this.eas = new ArrayList<Evolution>(problems.size());
		this.initialPopulation = initial;
		for(int i =0 ; i<problems.size(); i++)
			eas.add(new Evolution(problems.get(i),100,dimension,0.1,1000,initialPopulation));
	}
	
	public void iteration(){
		for(int i = 0; i<size; i++){
			solutions[i]=eas.get(i).iteration();
			System.out.println(problems.get(i).fitness(solutions[i])+" "+Arrays.toString(solutions[i]));
		}
	}
	
	public void printMembers(){
		for(Subproblem curr: problems)
			curr.print();
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
}
