package ea;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import mop.Subproblem;
import mop.Utility;


public class Evolution {
	public static int LOWER = 0;
	public static int UPPER = 1;
	private Subproblem problem;
	private List<double []> population;
	private List<double []> children;
	private List<double []> mutants;
	private int iterations = 0;
	private int dimension;
	private double epsilon;
	private int maxIterations;

	public Evolution(Subproblem problem, int size, int dimension, double epsilon, int maxIterations, List<double []> initial) {
		this.problem = problem;
		this.dimension = dimension;
		this.epsilon = epsilon;
		this.maxIterations = maxIterations;
		population = initial;
	}
	
	public Evolution(Subproblem problem, int size, int dimension, double epsilon, int maxIterations) {
		this.problem = problem;
		this.dimension = dimension;
		this.epsilon = epsilon;
		this.maxIterations = maxIterations;
		init(size);
	}
	
	public static List<double[]> generatePopulation(int size, int dimension){
		Random r = new Random();
		List<double []> pop = new ArrayList<double []>(size);
		for(int i = 0; i< size; i++){
			double [] tmp = new double[dimension];
			for(int j = 0; j<dimension; j++)
				tmp[j] = r.nextDouble() * (Evolution.UPPER - Evolution.LOWER) + Evolution.LOWER;
			pop.add(tmp);
		}
		return pop;
	}

	public double fitness(double [] input) {
		return problem.fitness(input);
	}

	public static double [] crossover(double [] mom, double [] dad) {
		int split = new Random().nextInt(mom.length);
		double [] child = new double [mom.length];
		for(int i = 0; i<mom.length; i++)
			if(i<split)
				child[i] = mom[i];
			else
				child[i] = dad[i];
		return child;
	}

	public static double [] mutate(double [] point) {
		Random r = new Random();
		int pos = r.nextInt(point.length);
		point[pos] = r.nextDouble() * (Evolution.UPPER - Evolution.LOWER) + Evolution.LOWER;
		return point;
	}

	public void init(int popsize) {
		Random r = new Random();
		this.population = new ArrayList<double []>(popsize);
		for (int i = 0; i < popsize; i++){
			double [] individual = new double[dimension];
			for(int j = 0; j < dimension; j++)
				individual[j] = r.nextDouble() * (Evolution.UPPER - Evolution.LOWER) + Evolution.LOWER;
			population.add(individual);
		}
	}

	public double [] iteration() {
		int size = this.population.size();
		double [] best = null;
		Random rand = new Random();
		
		while (best == null) {
			// init children
			children = new ArrayList<double []>(size);
			mutants = new ArrayList<double []>(size);
			for (int i = 0; i < size; i++) {
				int dadpos;
				do {
					dadpos = rand.nextInt(size);
				} while (dadpos == i);
//				System.out.println(i+" makes love with "+dadpos);
				children.add(Evolution.crossover(population.get(i),
						population.get(dadpos)));
			}
			// init mutants
			for (double [] child : children)
				mutants.add(Evolution.mutate(child));
			// compare
			population.addAll(mutants);
			population = Utility.findNearestNeighbors(population, size, new Comparator<double []>(){
				@Override
				public int compare(double[] arg0, double[] arg1) {
					double fitness1 = problem.fitness(arg0);
					double fitness2 = problem.fitness(arg1);
					
					if(fitness2 - fitness1 > 0)
						return -1;
					else if(fitness2 - fitness1 < 0)
						return 1;
					return 0;
				}
			});
			if(problem.fitness(population.get(0)) < this.epsilon || iterations > this.maxIterations)
				best = population.get(0);
			
//			System.out.println("Iteration "+iterations+": "+problem.fitness(population.get(0)));
			
			iterations++;
		}
		return best;
	}

	public int getIterations() {
		return iterations;
	}

	public void setIterations(int iterations) {
		this.iterations = iterations;
	}
}
