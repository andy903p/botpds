import java.util.ArrayList;


public class Griewank {
	
	int dimension = 4;
	int popsize = 10;
	
	public Particle iteration() {
		
		double gBestFitness = Double.MAX_VALUE;
		double[] gBest = new double[dimension];
		Particle best = null, previousBest = null;
		int unchanged = 0;
		
		ArrayList<Particle> swarm = new ArrayList<Particle>();
		
		for(int i = 0; i < popsize; i++) {
			swarm.add(i, new Particle(dimension));
			if(swarm.get(i).getpBestFitness() < gBestFitness) {
				for(int j=0; j<dimension; j++)
					gBest[j] = swarm.get(i).getpBest()[j];
				gBestFitness = swarm.get(i).getpBestFitness();
				if(best != null)
					previousBest = best;
				best = swarm.get(i);
			}
		}
		
		boolean done = false;
		int counter = 0;
		while(!done) {
			System.out.println("Iteration: " + counter + " Best Fitness: " + gBestFitness);
			for(int i = 0; i < popsize; i++)
				swarm.get(i).move(gBest);
			
			for(int i = 0; i < popsize; i++) {
				if(swarm.get(i).getpBestFitness() < gBestFitness) {
					for(int j=0; j<dimension; j++)
						gBest[j] = swarm.get(i).getpBest()[j];
					gBestFitness = swarm.get(i).getpBestFitness();
					if(best != null)
						previousBest = best;
					best = swarm.get(i);
				}
			}
			counter++;
			
			if(gBestFitness < 0.1) //|| unchanged > popsize*dimension)
				done = true;
			
			if(previousBest != null && previousBest.equals(best)){
				unchanged++;
			}
			else
				unchanged = 0;
			/*if(counter%100 == 0)
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
				
		}
		
		System.out.println("Done, needed " + counter + " iterations");
		for(int i = 0; i < dimension; i++)
			System.out.print(gBest[i] + ",");
		System.out.println();
		
		return best;
		
	}
	
	public static void main(String[] args) {
		Griewank g = new Griewank();
		System.out.println("THE BEST!!!!!: "+g.iteration().getpBestFitness());
	}
	
}
