import java.util.Arrays;
import java.util.Random;


public class Particle {
	
	private double[] pos;
	private double[] v;
	private int dim;
	private double[] pBest;
	private double fitness;
	private double pBestFitness = Double.MAX_VALUE;
	private double w;
	private double c1;
	private double c2;
	private double r1;
	private double r2;
	
	public Particle(int dim) {
		this.dim = dim;
		init();
	}
	
	private void init() {
		Random r = new Random();
		pos = new double[dim];
		v = new double[dim];
		Arrays.fill(v, 0.0);
		
		for(int i = 0; i < dim; i ++) {
			pos[i] = (r.nextDouble() * 1200.d) - 600.d;
		}
		System.out.println(Arrays.toString(pos));
		w = 0.4d;
		r1 = r.nextDouble();
		r2 = r.nextDouble();
		r1 = 1;
		r2 = 1;
		c1 = 2.d;
		c2 = 2.d;
		
		runFitness();
	}
	
	public double[] getpBest() {
		return pBest;
	}

	public void setpBest(double[] pBest) {
		this.pBest = pBest;
	}

	public double getpBestFitness() {
		return pBestFitness;
	}

	public void setpBestFitness(double pBestFitness) {
		this.pBestFitness = pBestFitness;
	}

	public void move(double[] gBest) {
		for(int i = 0; i < dim; i++) {
			v[i] = (w*v[i]) + c1*r1*(pBest[i] - pos[i]) + c2*r2*(gBest[i] - pos[i]);
			pos[i] += v[i];
		}
		//System.out.println(Arrays.toString(v));
//		Random r = new Random();
//		r1 = r.nextDouble();
//		r2 = r.nextDouble();
//		
		runFitness();
	}
	
	public double runFitness() {
		
		double auxS = 0;
		double auxP = 1;
		for (int i = 1; i <= dim; i++) {
			auxS += Math.pow(pos[i-1], 2)/4000.0;
			auxP *= Math.cos(pos[i-1]/Math.sqrt(i));
		}
		this.fitness = auxS - auxP + 1.0;
		if(this.fitness < this.pBestFitness) {
			this.pBest = this.pos;
			this.pBestFitness = this.fitness;
		}
		return this.fitness;
		
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(fitness);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Particle other = (Particle) obj;
		if (Double.doubleToLongBits(fitness) != Double
				.doubleToLongBits(other.fitness))
			return false;
		return true;
	}
	
	

}
