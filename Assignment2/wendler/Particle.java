package wendler;

import java.util.Random;

public class Particle {
	private Random r;
	private double w, c1, c2, r1, r2;
	private double v[]; 
	private double pos[];
	private double pBest[];
	private double fitness, bestFitness = Double.MAX_VALUE;

	public Particle(double w, double c1, double c2, int dimension){
		r = new Random();
		this.w = w;
		this.c1 = c1;
		this.c2 = c2;
		this.r1 = r.nextDouble();
		this.r2 = r.nextDouble();
		this.v = new double[dimension];
		this.pos = new double[dimension];
		this.pBest = new double[dimension];
		for(int i = 0; i<dimension; i++){
			this.v[i] = 0;
			this.pos[i] = r.nextDouble() * 1200.d - 600.d;
			this.pBest[i] = this.pos[i];
		}
	}
	
	public void updateVelocity(double [] gBest){
		r1 = r.nextDouble();
		r2 = r.nextDouble();
		for(int i = 0; i < gBest.length; i++) {
			v[i] *= w;
			v[i] += c1*r1*(pBest[i] - pos[i]);
			v[i] += c2*r2*(gBest[i] - pos[i]);
		}
	}
	
	public void updatePosition(){
		for(int i=0;i<pos.length;i++)
			pos[i] += v[i];
	}

	public Random getR() {
		return r;
	}

	public void setR(Random r) {
		this.r = r;
	}

	public double getW() {
		return w;
	}

	public void setW(double w) {
		this.w = w;
	}

	public double getC1() {
		return c1;
	}

	public void setC1(double c1) {
		this.c1 = c1;
	}

	public double getC2() {
		return c2;
	}

	public void setC2(double c2) {
		this.c2 = c2;
	}

	public double getR1() {
		return r1;
	}

	public void setR1(double r1) {
		this.r1 = r1;
	}

	public double getR2() {
		return r2;
	}

	public void setR2(double r2) {
		this.r2 = r2;
	}

	public double[] getV() {
		return v;
	}

	public void setV(double[] v) {
		this.v = v;
	}

	public double[] getPos() {
		return pos;
	}

	public void setPos(double[] pos) {
		this.pos = pos;
	}

	public double[] getpBest() {
		return pBest;
	}

	public void setpBest(double[] pBest) {
		this.pBest = pBest;
	}
	

	public double getFitness() {
		return fitness;
	}
	
	public double getBestFitness() {
		return bestFitness;
	}

	public void setFitness(double fitness) {
		if(fitness < this.bestFitness){
			for(int i = 0; i<pos.length; i++)
				pBest[i] = pos[i];
			this.bestFitness = fitness;
		}
		this.fitness = fitness;
		
	}
}
