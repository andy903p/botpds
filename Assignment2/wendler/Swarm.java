package wendler;

import java.util.ArrayList;
import java.util.List;

public class Swarm {
	private int size, dimension;
	private List<Particle> swarm;
	private Particle gBest = null;
	long iterations = 0;

	public Swarm(double w, double c1, double c2, int size, int dimension) {
		this.size = size;
		this.dimension = dimension;
		this.swarm = new ArrayList<Particle>(size);
		for (int i = 0; i < size; i++)
			swarm.add(new Particle(w, c1, c2, dimension));
	}

	public static double fitness(double[] pos) {
		double auxS = 0;
		double auxP = 1;
		double fitness = 0;
		int dim = pos.length;
		for (int i = 1; i <= dim; i++) {
			auxS += Math.pow(pos[i-1], 2);
			auxP *= Math.cos(pos[i-1]/Math.sqrt(i));
		}
		fitness = (auxS/4000.d) - auxP + 1.0;		
		return fitness;
	}

	public Particle iteration() {
		// find best
		for (Particle curr : swarm) {
			curr.setFitness(fitness(curr.getPos()));
			if (gBest == null
					|| curr.getBestFitness() < gBest.getBestFitness()) {
				gBest = curr;
			}
		}
		boolean end = false;
		while (!end) {
			// move
			for (Particle curr : swarm)
				curr.updateVelocity(gBest.getpBest());
			for (Particle curr : swarm)
				curr.updatePosition();
			// find best
			for (Particle curr : swarm) {
				curr.setFitness(fitness(curr.getPos()));
				if (gBest == null
						|| curr.getFitness() < gBest.getBestFitness()) {
					gBest = curr;
				}
			}
			if(gBest.getFitness() < 0.1)
				end = true;

			iterations++;
			//System.out.println("Iteration "+iterations+" best fitness: "+gBest.getFitness());
		}
		return gBest;
	}
	
	

	@Override
	public String toString() {
		return "Swarm [size=" + size + ", dimension=" + dimension + ", iterations="+iterations+"]";
	}

	public static void main(String [] args){
		long avg_iterations = 0;
		int its = 10;
		for(int i=0; i<its; i++){
			Swarm swarm = new Swarm(0.4,2,2,100,30);
			swarm.iteration();
			System.out.println(swarm+" terminated");
			avg_iterations += (swarm.getIterations()/its);
		}
		System.out.println("AVG iterations: "+avg_iterations);
	}

	private long getIterations() {
		return iterations;
	}
}
