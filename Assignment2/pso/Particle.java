package pso;

import java.util.Arrays;
import java.util.Random;


public class Particle {
	
	private double[] pos;
	private double[] v;
	private int dim;
	private double[] pBest;
	private double fitness;
	private double pBestFitness = Double.MAX_VALUE;
	private double w;
	private double c1;
	private double c2;
	private double r1;
	private double r2;
	private Random r = new Random();
	
	public Particle(int dim, int offset) {
		this.dim = dim;
		this.r = new Random(123822581345l+offset*123822581345l);
		init();
	}
	
	private void init() {
		pos = new double[dim];
		pBest = new double[dim];
		v = new double[dim];
//		Arrays.fill(v, 0.0);)
		
		for(int i = 0; i < dim; i++) {
			v[i] = 0;
			pos[i] = (r.nextDouble() * 1200.d) - 600.d;
			pBest[i] = pos[i];
		}
		System.out.println(Arrays.toString(pos));
		w = 0.4d;
		c1 = 2d;
		c2 = 2d;
		
		runFitness();
	}
	public double[] getPos(){
		return pos;
	}
	
	public double[] getpBest() {
		return pBest;
	}

	public void setpBest(double[] pBest) {
		this.pBest = pBest;
	}

	public double getpBestFitness() {
		return pBestFitness;
	}

	public void setpBestFitness(double pBestFitness) {
		this.pBestFitness = pBestFitness;
	}

	public void updateSpeed(double[] gBest) {
		r1 = r.nextDouble();
		r2 = r.nextDouble();
		for(int i = 0; i < dim; i++) {
			v[i] *= w;
			v[i] += c1*r1*(pBest[i] - pos[i]);
			v[i] += c2*r2*(gBest[i] - pos[i]);
		}
		//System.out.println(Arrays.toString(v));
	}
	
	public void updatePositionAndValidate() {
		for(int i = 0; i < dim; i++) {
			pos[i] += v[i];
		}		
		runFitness();
	}
	
	public double runFitness() {
		double auxS = 0;
		double auxP = 1;
		for (int i = 1; i <= dim; i++) {
			auxS += Math.pow(pos[i-1], 2);
			auxP *= Math.cos(pos[i-1]/Math.sqrt(i));
		}
		this.fitness = (auxS/4000.d) - auxP + 1.0;

		if(this.fitness < this.pBestFitness) {
			for(int i = 0; i<dim; i++)
				pBest[i] = pos[i];
			this.pBestFitness = this.fitness;
		}
		return this.fitness;
		
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(fitness);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Particle other = (Particle) obj;
		if (Double.doubleToLongBits(fitness) != Double
				.doubleToLongBits(other.fitness))
			return false;
		return true;
	}
	
	

}
