package pso;

import java.util.ArrayList;



public class Griewank {
	
	int dimension = 10;
	int popsize = 100;
	
	public Particle iteration() {
		
		Particle best = null, previousBest = null;
		int unchanged = 0;
		
		ArrayList<Particle> swarm = new ArrayList<Particle>();
		/**
		 * initialize particles and gBest
		 */
		for(int i = 0; i < popsize; i++) {
			Particle tmp = new Particle(dimension,i);
			swarm.add(i, tmp);
			if(best == null || tmp.getpBestFitness() < best.getpBestFitness()) {
				best = tmp;
			}
		}
		
		boolean done = false;
		int counter = 0;
		while(!done) {
			System.out.println("Iteration: " + counter + " Best Fitness: " + best.getpBestFitness());
			/**
			 * move the particles
			 */
			for(int i = 0; i < popsize; i++)
				swarm.get(i).updateSpeed(best.getpBest());
			for(int i = 0; i< popsize; i++)
				swarm.get(i).updatePositionAndValidate();
			
			/**
			 * update gBest
			 */
			for(int i = 0; i < popsize; i++) {
				if(swarm.get(i).getpBestFitness() < best.getpBestFitness()) {
					best = swarm.get(i);
				}
			}
			counter++;
			
			if(best.getpBestFitness() < 0.1)
				done = true;
			
//			if(previousBest != null && previousBest.equals(best)){
//				unchanged++;
//			}
//			else
//				unchanged = 0;
//			if(counter%100 == 0)
//				try {
//					for(int i = 0; i<popsize; i++)
//						System.out.println(Arrays.toString(swarm.get(i).getPos()));
//					System.out.println(Arrays.toString(gBest));
//					Thread.sleep(10000);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
				
		}
		
		System.out.println("Done, needed " + counter + " iterations");
		for(int i = 0; i < dimension; i++)
			System.out.print(best.getpBest()[i] + ",");
		System.out.println();
		
		return best;
		
	}
	
	public static void main(String[] args) {
		Griewank g = new Griewank();
		System.out.println("THE BEST!!!!!: "+g.iteration().getpBestFitness());
	}
	
}
